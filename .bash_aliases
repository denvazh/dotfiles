# Colors for ls
alias ls='ls -G'

# Aliases for typical and most frequent use-cases
alias config='git --git-dir=$HOME/.config.git/ --work-tree=$HOME'
alias cstg='GIT_DIR=$HOME/.config.git stg'
