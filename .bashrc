# Load settings for interactive shell only
[ -z "$PS1" ] && return

# Load stored settings
if [ -f "${HOME}/.scripts/load_all.sh" ]; then
	. "${HOME}/.scripts/load_all.sh"
fi
