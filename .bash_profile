# Import bashrc
if [ -f "${HOME}/.bashrc" ]; then
	. "${HOME}/.bashrc"
fi

# Import aliases
if [ -f "${HOME}/.bash_aliases" ]; then
	. "${HOME}/.bash_aliases"
fi

