if [ "$UID" == 0 ]; then
	export LANG="POSIX"
	export LC_ALL="C"
else
	export LANG="en_US.UTF-8"
	export LC_ALL="en_US.UTF-8"
fi
