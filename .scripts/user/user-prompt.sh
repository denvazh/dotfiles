export CLICOLOR=1
export GIT_PS1_SHOWCOLORHINTS=1

get_dir() {
	printf "%s" $(pwd | sed "s:$HOME:~:")
}

get_sha() {
	git rev-parse --short HEAD 2>/dev/null
}

if [ "$UID" == 0 ]; then
	export PROMPT_COMMAND='__git_ps1 "\[\e[1;31m\][\u@\h][\D{%Y/%m/%d %T}]\[\e[m\]\n[\w]" "\\\$: " "\n{%s $(get_sha)}"'
else
	export PROMPT_COMMAND='__git_ps1 "\[\e[1m\][\u@\h][\D{%Y/%m/%d %T}]\n[\w]\[\e[m\]" "\\\$: " "\n{%s $(get_sha)}"'
fi

