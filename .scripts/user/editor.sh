VIM_INSTALLED=$(command -v vim >/dev/null; echo $?)

if [ ${VIM_INSTALLED} -eq 0 ]; then
	VIM_PATH=$(command -v vim)
	alias vi=$VIM_PATH
	export EDITOR=$VIM_PATH
fi
