# Generic bash completion
if [ -f "/usr/local/etc/bash_completion" ]; then
	. "/usr/local/etc/bash_completion"
fi

# Bash completion for git
GIT_INSTALLED=$(command -v /usr/local/bin/git >/dev/null; echo $?)

if [ ${GIT_INSTALLED} -eq 0 ]; then
	if [ -f "/usr/local/share/git-core/contrib/completion/git-completion.bash" ]; then
		. "/usr/local/share/git-core/contrib/completion/git-completion.bash"
	fi
fi
