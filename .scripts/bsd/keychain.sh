### START-Keychain ###
KEYCHAIN_INSTALLED=$(command -v /usr/local/bin/keychain >/dev/null; echo $?)

if [ ${KEYCHAIN_INSTALLED} -eq 0 ]; then

	# Let re-use ssh-agent and/or gpg-agent between logins
	KEYS=$(find $HOME/.ssh -type f | egrep -vE ".pub|known_hosts")
	/usr/local/bin/keychain "${KEYS}"
	source $HOME/.keychain/$HOSTNAME-sh
fi
### End-Keychain ###

