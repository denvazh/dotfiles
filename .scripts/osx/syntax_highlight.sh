if [ -f `brew --prefix`/bin/src-hilite-lesspipe.sh ]; then
	export LESSOPEN="| `brew --prefix`/bin/src-hilite-lesspipe.sh %s"
	export LESS=' -R '
fi
