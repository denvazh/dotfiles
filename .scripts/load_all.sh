#
# Generic script to check for platform and load all corresponding
# scripts from there
#
# Written by Denis Vazhenin

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ] ; do SOURCE="$(readlink "$SOURCE")"; done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

SCRIPT_DIR=$DIR
DEBUG=false

getOS() {
	osname=$(uname -s)
	OSTYPE=Unknown
	case $osname in
	"FreeBSD") OSTYPE="bsd" ;;
	"Linux") OSTYPE="linux" ;;
	"Darwin") OSTYPE="osx" ;;
	esac

	echo $OSTYPE
}

PLATFORM=$(getOS)

# traversing platform specific dir and execute scripts from there
for config_platform in $(find "$SCRIPT_DIR/$PLATFORM" -mindepth 1 -type f); do
	[[ $DEBUG == true ]] && echo "Loading $config_platform"
	source $config_platform
done

# loading generic settings common for all platforms
for config_common in $(find "$SCRIPT_DIR/common" -mindepth 1 -type f); do
	[[ $DEBUG == true ]] && echo "Loading $config_common"
	source $config_common
done

# loading user specific settings
for config_user in $(find "$SCRIPT_DIR/user" -mindepth 1 -type f); do
	[[ $DEBUG == true ]] && echo "Loading $config_user"
	source $config_user
done

