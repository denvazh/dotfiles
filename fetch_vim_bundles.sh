#!/usr/bin/env bash

#
# This is a workaround script to fetch necessary vim plugins
# in case install with git submodules fails
#

# Get list
URLS=$(egrep "url" .gitmodules | sed -e 's/[ \t]*//g' | awk -F"=" '{print $2}')

# Go to directory where vim plugins should be stored
pushd $HOME/.vim/bundle

# Actually do the magic :)
for i in ${URLS[@]}; do
	echo "Fetching plugins from $i"
	git clone $i
done

# Go back to directory where script was executed
popd
